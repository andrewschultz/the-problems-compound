### What is this repository for? ###

* 2015 IFComp game, The Problems Compound
* Version 1 -- a post-comp is almost certain

### Reporting bugs ###

* Basically, anything goes, big or small. My rejecting your bugs doesn't mean I reject you! In fact, I may say, great bug but I need to defer it
* This is a great place to leave an issue if something pops up that is super high priority, or if something pops up that is low priority but you don't want to have to search through the transcript for it later
* It's nice if trivial stuff (quick fixes) is lumped into one issue, but it's not necessary
* "It just bugged me" is a perfectly relevant report
* a cut and paste of the current transcript is nice. You can TYPE TRANSCRIPT.TXT and then cut and paste from there if it ran off the screen.
* reproduction steps are obviously helpful. If you're able to reproduce in a separate instance, that is a big help, but I think it's best to use time working through the rest of the game. I can recreate what happened, or I'll be able to see what settings caused the bugs.
* I can always repurpose issues for something else and may defer too-tough ones

### What is the repository structure? ###

* Ideally, there will be a release build, a debug build and a beta build. You will want to use the beta build to take advantage of shortcuts. However, nearer release time, I will need testing on the release build to make sure no beta stuff slips in.
* release build = no help should for testers. You should be able to solve the game with a walkthrough.
* beta build = help for testers, but PURLOIN etc. blocked
* debug build = Inform's debug commands are included
* There will be a trizbort map and its PDF equivalent.
* The wiki may include test cases.
* There should be a link to the Google white paper.